# include<cstdlib>
# include<iostream>
# include<fstream>
# include<NTL/ZZ.h>
# include<NTL/tools.h>
# include<cstdlib>
# include<time.h>
# include<NTL/RR.h>
# include<NTL/ZZ_pX.h>
# include<NTL/mat_ZZ.h>
# include<NTL/vec_ZZ.h>
# include<NTL/LLL.h>
# include<NTL/ZZX.h>
# include<fstream>
# include <ctime>


using namespace std;
using namespace NTL;

const int dim=8;
const int m=128; // # of samples
ZZ pSIZE(16777216); //the value for p_size
ZZ e_max(3200);
ZZ e_min(457);
ZZ sk_max(229119); //2^17.8
RR qr=power(to_RR(2),64);
ZZ q=to_ZZ(qr);
ZZ k,kk; //coprime with q
//ZZ s[dim];
//ZZ ss[dim];
vec_ZZ s,ss;
ZZ w(224);
ZZ ww(32);
ZZ wwbb;
ZZ p,ck,ckk,sk,skk,ckskckkskk;
ZZ lu;

ZZ b(16);
ZZ bb(68719476736);
ZZ a[m][dim]; //ZZ_b
mat_ZZ publicA;
ZZ e[m];//[e_min,e_max]
ZZ ee[m];//[e_min,e_max]
ZZ u[m];//ZZ_bb
ZZ r[m];//ZZ_p
ZZ rr[m];
ZZ pk[m];
ZZ pkk[m];
ZZ k_inv_q;
ZZ kk_inv_q;
ZZ N;
ZZ NN;
vec_ZZ delta, ddelta;// r_i+e_i*p
vec_ZZ del_sub;  //found by CVP
vec_ZZ ddel_sub;
ZZ epr[m];
ZZ eprepr[m];
vec_ZZ PK;
vec_ZZ PKK;
vec_ZZ r_,rr_,epr_,eprepr_,u_,e_,ee_;
ZZ ckk_sub(1);
ZZ ck_sub;


//生成私钥
void private_key(){
    //cout<<"\n Generate Private Key:"<<endl;
    //cout<<"--------------------------"<<endl;
    //k and kk are uniformly sampled from ZZ_q, and coprime with q
    //cout<<"k and kk are uniformly sampled from ZZ_q, and coprime with q"<<endl;
    ZZ temp(0);
    while(IsZero(temp)){
        RandomBnd(k,q);
        temp=k%2;
    }
    temp=0;
    while(IsZero(temp)){
        RandomBnd(kk,q);
        temp=kk%2;
    }
    temp=0;
    
    s.SetLength(dim);
    ss.SetLength(dim);
    //s and ss : n-dim vector from ZZ_q[n]
    //cout<<"s and ss : n-dim vector randomly sampled from ZZ_q[n]"<<endl;
    for(int i=0;i<dim;i++){
        s[i]=RandomBnd(q);
        ss[i]=RandomBnd(q);
    }
    
    
    //p is randomly sampled from {(w+ww)bb,...,(w+ww)bb+pSIZE}
    //cout<<"p is randomly sampled from {(w+ww)bb,...,(w+ww)bb+pSIZE}"<<endl;
    wwbb=(w+ww)*bb;
    while(!IsOne(temp)){
        p=RandomBnd(pSIZE+1)+wwbb;
        temp=p%2;
        if((sk_max*bb+p+e_max*p)*(w+ww)>=q) temp=0;
    }
    //ck and ckk is randomly sampled from ZZ_p, and ckk is coprime with p
    //cout<<"ck and ckk is randomly sampled from ZZ_p, and ckk is coprime with p"<<endl;
    ck=RandomBnd(p);
    temp=0;
    while(!IsOne(temp)){
        ckk=RandomBnd(p);
        temp=GCD(ckk,p);
    }
    
    //sk and skk are randomly sampled from ZZ_sk_max, with sk*ck+skk*ckk coprime with p
    //cout<<"sk and skk are randomly sampled from ZZ_sk_max, with sk*ck+skk*ckk coprime with p"<<endl;
    sk=RandomBnd(sk_max);
    temp=0;
    while(!IsOne(temp)){ //until coprime
        skk=RandomBnd(sk_max);
        temp=GCD(sk*ck+skk*ckk,p);
    }

    
    cout<<"k = "<<k<<endl;
    cout<<"k'="<<kk<<endl;
    cout<<"p = "<<p<<endl;
    cout<<"ck = "<<ck<<endl;
    cout<<"ckk = "<<ckk<<endl;
    cout<<"sk = "<<sk<<endl;
    cout<<"skk = "<<skk<<endl;
    cout<<"s = "<<s<<endl;
    cout<<"ss = "<<ss<<endl;
    
    
        
    
}
//生成公钥
void public_key(){
    delta.SetLength(m);
    ddelta.SetLength(m);

    PK.SetLength(m);
    PKK.SetLength(m);

    for(int i=0;i<m;i++){
        for(int j=0;j<dim;j++)
            a[i][j]=RandomBnd(b);

        e[i]=RandomBnd(e_max-e_min+1)+e_min;
        ee[i]=RandomBnd(e_max-e_min+1)+e_min;
        u[i]=RandomBnd(bb);
        ZZ temp(1);
        
        r[i]=RandomBnd(p);
        ZZ x,y;
        x=InvMod(ckk, p);
        y=-ck*r[i];  //fatal error before, forget the negative sign!
        rr[i]=MulMod(x, y, p);
        
        epr[i]=r[i]+e[i]*p;
        eprepr[i]=rr[i]+ee[i]*p;
        
        k_inv_q=InvMod(k, q);
        kk_inv_q=InvMod(kk,q);
        ZZ t1,t2;
        for(int j=0;j<dim;j++){
            t1+=a[i][j]*s[j];
            t2+=a[i][j]*ss[j];
        }
        AddMod(pk[i],t1,k_inv_q*(sk*u[i]+r[i]+e[i]*p),q);
        AddMod(pkk[i],t2,kk_inv_q*(skk*u[i]+rr[i]+ee[i]*p),q);
        PK[i]=pk[i]%q;
        PKK[i]=pkk[i]%q;
        
        delta[i]=epr[i];
        ddelta[i]=eprepr[i];

        e_.SetLength(m);
        ee_.SetLength(m);
        r_.SetLength(m);
        rr_.SetLength(m);
        u_.SetLength(m);
        for(int i=0; i<m; i++){
            e_[i] = e[i];
            ee_[i] = ee[i];
            r_[i] = r[i];
            rr_[i] = rr[i];
            u_[i] = u[i];
        }
        
        publicA.SetDims(m,dim);
        for(int i=0; i<m; i++){
            for(int j=0; j<dim; j++) publicA[i][j] = a[i][j];
        }

    }
}
//生成加密是需要的l
vec_ZZ Gen_l(vec_ZZ& uu){
    vec_ZZ l;
    l.SetLength(m);
    ZZ pos_sum, neg_sum;
    long i,stop;
    pos_sum = RandomBnd(ww) +w ;// w,w+ww
    neg_sum = RandomBnd(ww); //0,ww


    l(1) = RandomBnd(neg_sum);

    for(i = 2; ; i++){
        neg_sum -= l(i-1);

        if(neg_sum == 1) 
        {
            stop = i;
            l(i)=1;
            //cout<<"l("<<i<<")="<<l(i)<<endl;
            //cout<<"stop index = "<<i<<endl;
            break;
        }
        //cout<<"rest sum = "<<neg_sum<<endl;
        l(i) = RandomBnd(neg_sum);
        //cout<<"l("<<i<<")="<<l(i)<<endl;
    }
    cout<<endl;
    for(i = 1; i<=stop; i++){
        l(i) = -l(i);
    }

    //end of generation of negative entries

    cout<<endl; lu=0;
    for(i=1;i<=stop;i++){
        lu += l(i)*uu(i);
    }

    int flag= 1;
    l(stop+1) = RandomBnd(pos_sum);
    lu += uu(i) * l(stop+1);
    while(flag){
        for( i = stop + 2; i <= m; i++){
            pos_sum -= l(i-1);
            if(pos_sum == 1){
                l(i) = 1;
                lu += 1;
                flag = 0;
                break;
            } 
            if( i == m){
                l(i) = pos_sum;
                 lu += l(i) * uu(i);
                 if(lu > 0){
                     flag = 0;
                      break;}
                 else continue;

            }
            l(i) = RandomBnd(pos_sum);
            lu += l(i) * uu(i);
        }
    }
    neg_sum = 0;
    for(i=1; i<=stop; i++){
        neg_sum += l(i);
    }
    cout<<"Sum of negative entries : "<< neg_sum <<" within ("<<-ww<<", 0)"<<endl;
    pos_sum = 0;
    for( i = stop + 1; i<= m; i++){
        pos_sum += l(i);
    }
    cout<<"Sum of positive entries : "<< pos_sum <<" within ("<<w<<","<<w+ww<<")"<<endl;
    cout<<"lu = "<<lu<<endl;
    cout<<" p = "<<p<<endl;
    cout<<"l = "<<l<<endl;
    return l;
    
}

void interval_shift(ZZ & old_val){
    if(old_val%q > q/2) old_val = old_val%q - q;
    else old_val = old_val%q;
}

ZZ norm_1(vec_ZZ& vv){
    long l= vv.length();
    ZZ sum;
    for(long i = 1; i<= l; i++){
        sum += abs(vv(i));
    }
    return sum;
}
//------------------------------------------------------------------------------------------



void ini_param(int tt=0){
    cout<<"q = 2^64 = "<<q<<endl;
    cout<<"sk_max="<<sk_max<<" < 2^18"<<endl;
    cout<<"e_max="<<e_max<<" < 2^12"<<endl;
    cout<<"b'= 2^36 = "<<bb<<endl;
    cout<<"p_size = 2^24 = "<<pSIZE<<endl;
        
    SetSeed(to_ZZ(time(nullptr)));


    if(tt == 0){
    private_key();
    
    public_key();
    }
    else if( tt == 1){
        k=conv<ZZ>("15734020504163163321");
        kk=conv<ZZ>("3359329212410575711");
        p = conv<ZZ>("17592187424279");
        ck_sub = conv<ZZ>("12973411347097");
        ckskckkskk = conv<ZZ>("471931166234");
    }
    
}

/*
 We  want to obtain the linear combinations annihilating the a1,...,a128
 and u1,...u128 at the same time.
 
 */
/*
 Method 1: NS97
 b1=a1,u1
 ...
 b128=a128,u128
 
 # of entries in bi = n+1 = 9
 # of lattice generators = m =128
 
 L = {x in Z^128 : x[A u]=0 mod q}
 
 det(L) = q^{# of entries of x} = q^m = q^128
 
 Select c=2^{}
 
 */

void rec_k_kk_p_ck_ckk(){
    N = to_ZZ(power(to_ZZ(2), (dim+m)/2+m*(m-1)/4)*power(q, m));
    mat_ZZ X,Y;
    X.SetDims(dim+1+m,dim+1+m);
    Y.SetDims(m,dim+1);

    /*
     X =
     [ N(A,u)    I_m ]
     [ NqI_{n+1}  0  ]
     */
    int i,j;
    for(i=1;i<=m;i++){
        
        for(j=1;j<=dim;j++){
            X(i,j)=N*a[i-1][j-1];
            Y(i,j)=a[i-1][j-1];
        }
        
        X(i,j)=N*u[i-1];
        Y(i,j)=u[i-1];
        X(i,j+i)=1;
    }
    for(i=m+1;i<=m+1+dim;i++){
        X(i,i-m)=N*q;
    }
    
    mat_ZZ X_reduced(X);
    BKZ_RR(X_reduced);
    
    /*
     [  0   ..ONE.. ]
     [.TWO. .THREE. ]
     */
    cout<<"[ N(A,u)    I_m ]"<<endl;
    cout<<"[ NqI_{n+1}  0  ]"<<endl;
    cout<<"=========reduced========="<<endl;
    
    cout<<"[  0   ..ONE.. ]"<<endl;
    cout<<"[.TWO. .THREE. ]"<<endl;
    
    mat_ZZ alph; /*[ONE]*/
    alph.SetDims(m,m);
    for(int i=1;i<=m;i++){
        for(int j=1;j<=m;j++){
            alph(i,j)=X_reduced(i,dim+1+j);
        }
    }
    //cout<<"alph:"<<alph(1)<<endl;
    
    vec_ZZ sum; //norm-1
    sum.SetLength(m);
    
    
    for(int i=1;i<=m;i++){
        for(int j=dim+2;j<=dim+1+m;j++){
            sum[i-1]+=abs(X_reduced(i,j));
        }
        // cout<<"norm1 "<<i<<"th = "<<sum[i-1]<<endl;
    }
    cout<<"norm1 of the 119th collected vector is "<<sum[118]<<endl;
    cout<<"norm1 of the 120th collected vector is "<<sum[119]<<endl;
    
    /* mm: number of pieces to be collected */
    const int mm=119;
    
    /* [collected_dot_pk]    */
    /* [     qI_128     ]    */
    vec_ZZ collected_dot_pk, collected_dot_delta;
    collected_dot_pk.SetLength(mm);
    collected_dot_delta.SetLength(mm);
    
    vec_ZZ collected_dot_pkk, collected_dot_ddelta;
    collected_dot_pkk.SetLength(mm);
    collected_dot_ddelta.SetLength(mm);
    
    for(int i=1;i<=mm;i++){
        
        InnerProduct(collected_dot_pk(i),alph(i), PK);
        InnerProduct(collected_dot_pkk(i),alph(i), PKK);
        InnerProduct(collected_dot_delta(i),alph(i),delta);
        InnerProduct(collected_dot_ddelta(i),alph(i),ddelta);
        
        //  if(collected_dot_delta(i)<0)
        //   collected_dot_delta(i) = q + collected_dot_delta(i);
        
        //  if(collected_dot_ddelta(i)<0)
        //  collected_dot_ddelta(i) = q + collected_dot_ddelta(i);
        /* collected_dot_pk(i)=abs(collected_dot_pk(i));
         collected_dot_pkk(i)=abs(collected_dot_pkk(i));
         collected_dot_delta(i)=abs(collected_dot_delta(i));
         collected_dot_ddelta(i)=abs(collected_dot_ddelta(i));*/
        //     if(i<20)
        //cout<<"k(X,PK) mod q - (X,delta) = " <<k*collected_dot_pk(i)%q-collected_dot_delta(i)<<endl;
    }
    
    /* ---------------------------------------------------------------------------- */
    /* Next, we are gonna recover k,k' 恢复k和k’ */
    
    /* [collected_dot_pk] */
    /* [     qI_119     ] */
    mat_ZZ collected_pk_qI;
    //first line is the inner product of collected vectors and PKs
    collected_pk_qI.SetDims(mm+1,mm);
    collected_pk_qI(1)=collected_dot_pk;
    
    /* [collected_dot_pkk] */
    /* [     qI_119     ] */
    mat_ZZ collected_pkk_qI;
    //first line is the inner product of collected vectors and PKKs
    collected_pkk_qI.SetDims(mm+1,mm);
    collected_pkk_qI(1)=collected_dot_pkk;
    
    
    //  mm=119;
    for(long t=1;t<=mm;t++){
        collected_pk_qI(1+t,t)=q;
        collected_pkk_qI(1+t,t)=q;
        
    }
    
    
     
     mat_ZZ REDUCED_collected_pk_qI(collected_pk_qI);
     
     mat_ZZ REDUCED_collected_pkk_qI(collected_pkk_qI);
     
     cout<<"The matrix to be reduced is of the form"<<endl;
     cout<<"[collected_dot_pk]"<<endl;
     cout<<"[     qI_119     ]"<<endl;
     
     cout<<"Doing BKZ on collected_pk_qI and collected_pkk_qI "<<endl;
     BKZ_RR(REDUCED_collected_pk_qI,0.99,15);
     BKZ_RR(REDUCED_collected_pkk_qI,0.99,15);
     
     //cout<<"After BKZ, the matrix is of the form"<<endl;
     //cout<<"[                    0                      ]"<<endl;
     //cout<<"[k(x^(1),PK)+qt_1 ...  k(x^(119),PK)+qt_119 ]"<<endl;
     //cout<<"[                 qI_119                    ]"<<endl;
     


    //---------------------------------recover k, k'---------------------------------------
    
     ZZ a,b,a_,b_;
     
     for(long i=1;i<=119;i++){
     if(IsOdd(collected_dot_pk(i))){
     cout<<"the "<<i<<"-th entry is the first one to be odd"<<endl;
     a=REDUCED_collected_pk_qI(2,i);
     b=collected_dot_pk(i);
     
     cout<<"k*pk_"<<i<<"="<<a<<"(mod q)"<<endl;
     cout<<"pk_"<<i<< "="<<b<<"(mod q)"<<endl;
     break;
     }
     if(i==119){
             cout<<"inverse does not exist."<<endl;
             exit(EXIT_FAILURE);}     }
     
     
     for(long i=1;i<=119;i++){
     if(IsOdd(collected_dot_pkk(i))){
     a_=REDUCED_collected_pkk_qI(2,i);
     b_=collected_dot_pkk(i);
     
     //the fisrt entry of the second vector of the reduced matrix, aka k'*pk'_1
     cout<<"the "<<i<<"-th entry is the first one to be odd"<<endl;
     
     cout<<"k'*pk'_"<<i<<"="<<a_<<"(mod q)"<<endl;
     cout<<"pk'_"<<i<< "="<<b_<<"(mod q)"<<endl;
     break;
     }
         if(i==119){
             cout<<"inverse does not exist."<<endl;
         exit(EXIT_FAILURE);}
     }
     // a_=REDUCED_collected_pkk_qI(2,1);
     // b_=collected_dot_pkk(1);
    
    
     cout<<"To compute k*pk*(pk^{-1} mod q) mod q to obtain k"<<endl;
     
     
     ZZ c,c_,k_rec,kk_rec;
     InvMod(c,b%q,q);
     InvMod(c_,(b_)%q,q);
     cout<<"===================="<<endl<<endl;
     
     
     k_rec = a*c%q;
     kk_rec = (a_)*(c_)%q;
     
     cout<<"k="<<k<<endl;
     cout<<"k="<<k_rec<<" recovered"<<endl;
     if(!(k==k_rec))
     cout<<"k="<<q - k_rec<<" q - k_rec"<<endl;
     //cout<<"k recovered: "<<a*c%q<<endl;
     
     cout<<"k'="<<kk<<endl;
     cout<<"k'="<<kk_rec<<" recovered"<<endl;
     if(!(kk==kk_rec))
     cout<<"k'="<<q - kk_rec<<" q - kk_rec"<<endl;
    
    
    //----------------------------------recover p------------------------------------
    /*
     The second row is supposed to be equal to [(x^(1),delta) ... (x^(119),delta)]
     Use this row to compute the resulting determinant.
     */
    
     vec_ZZ k_X_PK(REDUCED_collected_pk_qI(2));
     vec_ZZ kk_X_PKK(REDUCED_collected_pkk_qI(2));
     
     
     
     const long _n_det=40;
     mat_ZZ Z;
     
     Z.SetDims(2,2);
     
     vec_ZZ det_vec;
     det_vec.SetLength(_n_det);
     
     vec_ZZ gcd_vec;
     gcd_vec.SetLength(_n_det/2);
     
     for(long i = 1; i < _n_det; i++){
     
     // Z(1,1)=k*collected_dot_pk(i)%q;
     // Z(1,2)=kk*collected_dot_pkk(i)%q;
     // Z(2,1)=k*collected_dot_pk(i+1)%q;
     // Z(2,2)=kk*collected_dot_pkk(i+1)%q;
     
     Z(1,1) = k_X_PK(i);
     Z(2,1) = kk_X_PKK(i);
     Z(1,2) = k_X_PK(i+1);
     Z(2,2) = kk_X_PKK(i+1);
     det_vec(i) = determinant(Z);
     }
     
     //cout<<"gcd(det(x,delta),p) = "<<GCD(Det_vec(1),p)<<endl;
     gcd_vec(1)=det_vec(1);
     
     cout<<"possible p"<<endl;
     //cout<<gcd_vec(1)<<endl;
     
     for(long j=1; j < _n_det/2; j++){
     gcd_vec(j+1) = GCD(gcd_vec(j), det_vec(j+1));
     }
     cout<<gcd_vec(_n_det/2)<<endl;
     cout<<"real p\n"<<p<<endl;
     
    //-------------------------------recover ck, ck'-------------------------------------
    
    //set ckk = 1, ck = - (x^(i),ddelta)(x^(i),delta)^{-1}

    ZZ some_inv;
    for(long i = 1; i<120; i++){
        if(GCD(collected_dot_delta(i), p)==1){
            InvMod(some_inv,collected_dot_delta(i)%p,p);
            ck_sub = (- collected_dot_ddelta(i) * some_inv)%p;
            break;
        }
    }
    InvMod(some_inv,ckk%p,p);
    ZZ ratio((ck*some_inv)%p);
    cout<<"substituted ck' = "<<ckk_sub<<endl;
    cout<<"substituted ck = "<<ck_sub<<endl;
    cout<<"ratio = ck/ck' mod p = "<<ratio<<endl;
    
    cout<<"real: ck*sk + ck'*sk' mod p = "<< (sk*ck + skk*ckk) % p<<endl;
    cout<<"ck_sub * sk + 1 * sk' mod p = "<< (ck_sub * sk + skk) % p <<endl;

    
    
}

void more_about_skskk(){
    
    NN = to_ZZ(power(to_ZZ(2), (dim+m-1)/2+(m-2)*(m-1)/4)*power(q, m));
    
    mat_ZZ XX;
    
    
    XX.SetDims(dim+m, dim + m);
    
    const int mm=119;
    int i,j;
    
    /*
     XX =
     [ NA    I_m ]
     [ NqI_n  0  ]
     */
    for(i=1; i <= m; i++){
        for(j = 1; j <= dim; j++ ){
            XX(i,j) = NN*a[i-1][j-1];
        }
        XX(i, dim+i) = 1;
    }
    for(i=m+1; i<=m+dim; i++){
        XX(i,i-m)=q*NN;
    }
    
    mat_ZZ XX_reduced(XX);
    BKZ_RR(XX_reduced);
    
    
    mat_ZZ beta;
    beta.SetDims(m,m);
    for(i=1; i<= m; i++){
        for(j=1; j<=m; j++){
            beta(i,j)=XX_reduced(i,dim+j);
        }
    }
    vec_ZZ sum_nulla;
    sum_nulla.SetLength(m);
    vec_ZZ vec_u;
    vec_u.SetLength(m);
    ZZ u_dot_x;
    for(i=1; i<=m; i++) vec_u(i)=u[i-1];
    cout<<"Computing norm 1"<<endl;
    for(i=1; i<=m; i++){
        for(j=1; j<=m; j++){
            sum_nulla(i) += abs(beta(i,j));
        }
        InnerProduct(u_dot_x,beta(i),vec_u);
        //cout<<"norm 1 of "<<i<<"-th null vector: "<<sum_nulla(i)<<endl;
        //cout<<"u dot null vector x: "<<u_dot_x<<endl;
    }
    //norm 1 of beta
    
    
    
    ZZ col_dot_pk, col_dot_pkk,col_dot_u,inv_col_dot_u, real_sc;
    
    ZZ temp;// record computed sk*ck + skk*ckk mod p
    
    
    
    for(i =1; i<= mm; i++){
        InnerProduct(col_dot_u,beta(i),vec_u);
        if(GCD(col_dot_u,p)!=1) continue;
        
        InnerProduct(col_dot_pk,beta(i),PK);
        col_dot_pk *= k;
        interval_shift(col_dot_pk);  //[-q/2 , q/2]
        
        InnerProduct(col_dot_pkk,beta(i),PKK);
        col_dot_pkk *= kk;
        interval_shift(col_dot_pkk);
        
        temp = ck_sub * (col_dot_pk) + ckk_sub* (col_dot_pkk) ;
        temp = temp % p;
        
        InvMod( inv_col_dot_u, col_dot_u%p, p);
        temp = (inv_col_dot_u * temp) % p;
        real_sc = (ck*sk + ckk*skk)%p;
        
        cout<<"computed: ck*sk + ck'*sk' mod p = "<<temp<<endl;  //ck ckk用替代的，sk skk用真实的，也满足这个关系式
        cout<<"ck' * computed mod p = "<<( temp * ckk ) % p<<endl; //这个等于真实的 ck*sk + ckk*skk的值


        break;
    }
    ckskckkskk = temp;
    
    mat_ZZ A_ins;
    A_ins.SetDims(3,1);
    A_ins(1,1) = ck_sub;
    A_ins(2,1) = 1;
    A_ins(3,1) = p;
    vec_ZZ y_ins, x_ins;
    y_ins.SetLength(1);
    x_ins.SetLength(3);
    y_ins(1) = temp;
    LatticeSolve(x_ins, A_ins, y_ins, 1);
    //special solution for ck_sub * x1 + x2 + p * x3 = temp
    cout<<"special solution to ck_sub * x1 + x2 + p * x3 = known val is "<< x_ins <<endl;
    
    vec_ZZ special_skskk;
    special_skskk.SetLength(2);
    special_skskk(1) = x_ins(1);
    special_skskk(2) = x_ins(2);
    // kernel lattice {(x1,x2) : ck_sub * x1 + x2 = 0 mod p}
    //(x1 x2 x3)(ck_sub 1 p)^T = 0 mod p
    mat_ZZ one,B_ins;
    B_ins.SetDims(2,1);
    B_ins(1,1) = ck_sub;
    B_ins(2,1) = 1;
    one.SetDims(2,2);
    ZZ c;
    c= 4*p*p;
    mat_ZZ pin,zero_basis;
    pin.SetDims(3,3);
    zero_basis.SetDims(2,2);
    pin(1,1) = c*ck_sub;
    pin(2,1) = c*1;
    pin(3,1) = c*p;
    pin(1,2) = 1;
    pin(2,3) = 1;
    BKZ_RR(pin);
    zero_basis(1,1) = pin(1,2); zero_basis(1,2) = pin(1,3);
    zero_basis(2,1) = pin(2,2); zero_basis(2,2) = pin(2,3);
    cout<<"kernel lattice basis"<<endl;
    cout<<zero_basis<<endl;

    //kernel_mod_lattice(one,B_ins,p);
    //one record the kernel lattice basis
    vec_ZZ close_to_special;
    close_to_special.SetLength(2);
    NearVector( close_to_special, zero_basis, special_skskk);
    vec_ZZ hope_short_skskk(special_skskk - close_to_special);
    cout<<" short possible [ sk, sk'] = "<<hope_short_skskk<<endl;
    cout<<" real [ sk, sk'] = ["<<sk<<","<<skk<<" ]"<<endl;
    

}

void shorten_delta(){
    ofstream fout;
    vec_ZZ tt;  //find a vector in a lattice
    long i,j;
    tt.SetLength(2*m);
    for(i=1; i<=m; i++){
        tt(i) = k * PK(i) - sk * u_(i) ;
        tt(i) = tt(i) % q;
        tt(i+m) = kk * PKK(i) - skk * u_(i);
        tt(i+m) = tt(i+m)%q;
    }
    /* A = 
    [ a1^T a2^T ... a128^T                    ]
    [                     a1^T a2^T ... a128^T]
    */
    mat_ZZ double_A;
    double_A.SetDims(2*dim, 2*m);
    cout<<"A is "<<2*dim<<" * "<<2*m<<endl;
    for(j = 1; j <= m; j++ ){
        for( i = 1; i <= dim; i++){
            double_A(i,j) =  publicA(j,i);
            double_A( dim + i, m + j) = double_A(i,j);
        }
    }
    /*
    [ck                 ]
    [   ck              ]
    [        ...        ]
    [                 ck]
    [ck'                ]
    [   ck'             ]
    [        ...        ]
    [                ck']
    [        pI_128     ]
    {(delta1,...,delta128,...,ddelta1,...,ddelta128) : ck * delta_i + ckk * ddelta_i = 0 mod p }
    */

    mat_ZZ decline_ckckk; 
    decline_ckckk.SetDims(3*m, m);
    for( j = 1; j <= m; j++ ){
        decline_ckckk(j,j) = ck;
        decline_ckckk( m + j, j ) = ckk;
        decline_ckckk( 2*m + j, j ) = p;
    }

    mat_ZZ U;
    U.SetDims(3*m,3*m);
    long dec_rank;
    cout<<"Find decline_ckckk kernel basis"<<endl;
    dec_rank = LLL_FP(decline_ckckk,U);
    cout<<"Success"<<endl;
    // 3m - dec_rank rows of U are kernel lattice basis 

    mat_ZZ zero_dec; //B
    zero_dec.SetDims(3*m - dec_rank, 2*m);
    for( i = 1; i <= 3*m - dec_rank; i++){
        for( j = 1; j <= 2*m; j++){
            zero_dec(i,j) = U(i,j);
        }
    }
    fout.open("zero_dec");
    fout<<zero_dec<<endl;
    fout.close();
    cout<<"decline_ck kernel basis is (3m-r)*2m (aka "<<3*m - dec_rank<<","<<2*m<<")"<<endl;
    
    vec_ZZ SZT;  //linear combination
    SZT.SetLength(2*dim + (3*m - dec_rank) + 2*m );
    /*
    for( i = 1; i <= dim; i++){
        SZT(i) = s(i);
        SZT(i+dim) = ss(i);
    }*/

    //for given A and y, there exists x such that x*A = y;
    /* 2m columns
    A   [double_A]
      = [zero_dec]
        [  qI_2m ]   
    y = tt

    goal: zB = (delta1, delta2, ..., delta128, ddelta1, ...,ddelta128)
    short enough to do the equivalent decryption
    */

/*
AA = [A    ]
   = [B    ]
   = [qI_2m]
*/

   mat_ZZ AA;
   AA.SetDims(2*dim + (3*m - dec_rank) + 2*m, 2*m );
   for( long col=1; col <= 2*m; col++){
       long h;
       for(h=1; h <= 2*dim; h++){
           AA(h,col) = double_A(h,col);
       }
       for(h=2*dim + 1; h <= 2*dim + (3*m - dec_rank); h++){
           AA(h,col) = zero_dec(h - 2*dim ,col);
       }
       AA(col + 2*dim + (3*m - dec_rank),col) = q;
   }


   mat_ZZ AA_reduced(AA);
   mat_ZZ V;
   V.SetDims(AA.NumRows(), AA.NumRows());

   long r;
   // current date/time based on current system
    time_t now0 = time(0);
    // convert now to string form
    char* dt0 = ctime(&now0);
    cout << "The local date and time is: " << dt0 << endl;
    
    cout<<"Find special solution"<<endl;
    LatticeSolve(SZT, AA, tt);
    cout<<"Success"<<endl;
    // current date/time based on current system
    time_t now1 = time(0);
    // convert now to string form
    char* dt1 = ctime(&now1);
    cout << "The local date and time is: " << dt1 << endl;
    
    vec_ZZ s0,z0; //special solution
    z0.SetLength(3*m - dec_rank);
    for( i = 1; i<= 3*m - dec_rank; i++){
        z0(i) = SZT(2*dim + i);
    }
    s0.SetLength(2*dim);//(k*s1,...,k*s8,kk*s1,...,kk*s8)
    for(i=1; i<=2*dim; i++){
        s0(i) = SZT(i);
    }
    //cout<<"s0 = "<<s0<<endl;
    //cout<<"z0 = "<<z0<<endl;

    //{(S,Z,T) : 0 = (S,Z,T)AA}'s basis
    cout<<"Find A B qI kernel basis"<<endl;
    r = LLL_FP(AA_reduced, V);
    cout<<"Success"<<endl;
    mat_ZZ K;

    K.SetDims(AA.NumRows()-r, AA.NumRows());
    cout<<"K is "<<K.NumRows()<<" * "<<K.NumCols()<<endl;

    for(i=1; i<=AA.NumRows()-r; i++){
        for(j=1; j<=AA.NumRows();j++){
            K(i,j) = V(i,j);
        }
    }
    // K = (K1 K2 K3)
    mat_ZZ K1,K2;
    K1.SetDims(K.NumRows(),2*dim);
    K2.SetDims(K.NumRows(),(3*m - dec_rank));
    cout<<"K2 is "<<K2.NumRows()<<" * "<<K2.NumCols()<<endl;

    for(i=1;i<=K.NumRows();i++){
        for(j=1; j<=2*dim;j++){
            K1(i,j) = K(i,j);
        }
        for(j= 1; j<= (3*m - dec_rank); j++ ){
            K2(i,j) = K(i, 2*m + j);
        }
    }
    
    fout.open("K1");
    fout<<K1<<endl;
    fout.close();
    fout.open("K2");
    fout<<K2<<endl;
    fout.close();
    
    vec_ZZ z0B;
    z0B.SetLength(2*m);
    fout.open("z0B");
    z0B = z0 * zero_dec;
    for(i=1; i<=15;i++) cout<<z0B(i)<<"\t";
    cout<<endl;
    fout<<z0B<<endl;
    fout.close();


    mat_ZZ K2B; //basis 272*256
    K2B.SetDims(2*dim + 2*m,2*m);
    cout<<"zero_dec is "<<zero_dec.NumRows()<<" * "<<zero_dec.NumCols()<<endl;
    mul(K2B,K2,zero_dec);
    cout<<"K2B is "<<K2B.NumRows()<<" * "<< K2B.NumCols()<<" matrix."<<endl; 
    fout.open("K2B_before_LLL");
    fout<<K2B<<endl;
    fout.close();

    long r_of_K2B;
    cout<<"Doing LLL on K2B"<<endl;
    // current date/time based on current system
    time_t now = time(0);
    // convert now to string form
    char* dt = ctime(&now);
    cout << "The local date and time is: " << dt << endl;


    r_of_K2B = LLL_RR(K2B);
    cout<<"End of LLL"<<endl;
    time_t then = time(0);
    char* td = ctime(&then);
    cout << "The local date and time is: " << td << endl;
    
    cout<<"rank of K2B is "<<r_of_K2B<<endl;
    fout.open("K2B_after_LLL");
    fout<<K2B<<endl;
    fout.close();


    mat_ZZ squareK2B;
    squareK2B.SetDims(2*m,2*m);
    for(j=1;j<=2*m;j++){
        for(i=1; i <= 2*m; i++){
            squareK2B(i,j) = K2B(i+2*dim,j);
        }
    }      
    
    vec_ZZ near_lvector; //near lattice vector
    cout<<"Doing CVP: find close vector in lattice sqaureK2B to target vector z0B"<<endl;
    near_lvector.SetLength( 2*m );
    /* B must be a square matrix, and it is assumed that B is already LLL or BKZ reduced (the better the reduction the better the approximation).*/
   NearVector( near_lvector, squareK2B, z0B);  
        cout<<"End of CVP"<<endl;
        cout<<"near vector"<<near_lvector<<endl;

    // !!!!!!!!!!! 这里不能直接用NearVector
    // near_vector = x * K2B, x is linear combination
    //(delta, ddelta) = z0 * B - near_vector, make it short by CVP
    //(delta, ddelta) = target - near_vector
    
    del_sub.SetLength(m);
    ddel_sub.SetLength(m);
    vec_ZZ double_delta( z0B - near_lvector );
    cout<<"double delta"<<double_delta<<endl;
    for(i=1; i<=m; i++){
        del_sub(i) = double_delta(i);
        ddel_sub(i) = double_delta(m+i);
    }
    

    vec_ZZ l(Gen_l(u_));
    ZZ d1,dd1;
    ZZ in1,in2,inn2;
    InnerProduct(in1, l, u_);
    InnerProduct(in2, l, del_sub);
    InnerProduct(inn2, l, ddel_sub);
    d1 = sk*in1 + in2;
    dd1 = skk*in1 + inn2;
    cout<<"q = "<<q<<endl;
    cout<< "sk(l,u) + (l,delta) = "<<d1<<endl;
    cout<<"sk'(l,u) + (l,delta') = "<<dd1<<endl; 
    

    /*
    vec_ZZ x; //读出系数
    x.SetLength(K2B.NumRows());
    LatticeSolve(x,K2B,-near_lvector);
    vec_ZZ s_sub,ss_sub;
    vec_ZZ s0_plus_xK1(s0 + x*K1);
    s_sub.SetLength(dim);
    ss_sub.SetLength(dim);
    for(i=1; i<=dim; i++){
        s_sub(i) = (InvMod(k,q)*s0_plus_xK1(i)) % q;
        ss_sub(i) = (InvMod(kk,q)*s0_plus_xK1(i+dim)) %q;
    }
    cout<<"subsitituted s"<<s_sub<<endl;
    cout<<"substituted ss"<<ss_sub<<endl;

   */ 
    
//sk(18)ui(36) + r(45) + ei(12)p(45)
//54 + 57
//8
//delta must be small


}

void print_param(){
    ofstream fout;
   fout.open ("param1.txt");

    fout<<"k = conv<ZZ>(\""<<k<<"\");"<<endl;
    fout<<"kk = conv<ZZ>(\""<< kk <<"\");"<<endl;
    fout<<"p = conv<ZZ>(\""<< p <<"\");"<<endl;
    fout<<"ck = conv<ZZ>(\""<< ck <<"\");"<<endl;
    fout<<"ckk = conv<ZZ>(\""<< ckk <<"\");"<<endl;
    fout<<"ckskckkskk = conv<ZZ>(\""<< ckskckkskk <<"\");"<<endl;
    fout<<"sk = conv<ZZ>(\""<< sk <<"\");"<<endl;
    fout<<"skk = conv<ZZ>(\""<< skk <<"\");"<<endl;
    fout.close();

    fout.open("param2.txt");
    fout<<"PK  PKK s  ss  r  rr  e  ee  u"<<endl;
    fout<<PK<<"\n\n"<<endl;
    fout<<PKK<<"\n\n"<<endl;
    fout<<s<<"\n\n"<<endl;
    fout<<ss<<"\n\n"<<endl;
    fout<<r_<<"\n\n"<<endl;
    fout<<rr_<<"\n\n"<<endl;
    fout<<e_<<"\n\n"<<endl;
    fout<<ee_<<"\n\n"<<endl;
    fout<<u_<<"\n\n"<<endl;
    fout.close();

    fout.open("matrix_A.txt");
    fout<<publicA<<endl;
    fout.close();
}

void param4test(){
k = conv<ZZ>("1557176281241667481");
kk = conv<ZZ>("5255408063148847025");
p = conv<ZZ>("17592195035153");
ck = conv<ZZ>("5240905662094");
ckk = conv<ZZ>("5374953890190");
ckskckkskk = conv<ZZ>("13521349461693");
sk = conv<ZZ>("71001");
skk = conv<ZZ>("227471");
ifstream fin;
fin.open("u");
fin>>u_;
fin.close();
fin.open("matrix_A.txt");
fin>>publicA;
fin.close();
fin.open("param2.txt");
//cout<<"s = "<<s<<endl;cout<<"ss = "<<ss<<endl;
fin>>PK>>PKK;
fin.close();   

}

void testCVP(){
    ifstream fin;
    long i,j;
    fin.open("K2B_after_LLL");
    mat_ZZ K2B;
    fin>>K2B;
    fin.close();
    fin.open("z0B");
    vec_ZZ z0B;
    fin>>z0B;
    fin.close();
    mat_ZZ squareK2B;
    squareK2B.SetDims(2*m,2*m);
    for(j=1;j<=2*m;j++){
        for(i=1; i <= 2*m; i++){
            squareK2B(i,j) = K2B(i+2*dim,j);
        }
    }      
    
    vec_ZZ near_lvector; //near lattice vector
    cout<<"Doing CVP: find close vector in lattice sqaureK2B to target vector z0B"<<endl;
    near_lvector.SetLength( 2*m );
    /* B must be a square matrix, and it is assumed that B is already LLL or BKZ reduced (the better the reduction the better the approximation).*/
   NearVector( near_lvector, squareK2B, z0B);  
        cout<<"End of CVP"<<endl;
        cout<<"near vector"<<near_lvector<<endl;
}


//------------------------------------------------------------------------------------------
int main(){
    //param4test();
    /*
    cout<<"A = "<<publicA<<endl;
    cout<<"-------------------------------"<<endl;
    cout<<"u = "<<u_<<endl;
    cout<<"k = "<<k<<endl;
    cout<<"k'="<<kk<<endl;
    cout<<"p = "<<p<<endl;
    cout<<"ck = "<<ck<<endl;
    cout<<"ckk = "<<ckk<<endl;
    cout<<"sk = "<<sk<<endl;
    cout<<"skk = "<<skk<<endl;
    cout<<"PK = "<<PK<<endl;
    cout<<"PKK = "<<PKK<<endl;
    */
    
    ini_param();
    rec_k_kk_p_ck_ckk();
    more_about_skskk();
    print_param();
    
      shorten_delta();

   

   
    
}
//TODO: 把上一轮的值给记录下来，下次就不用再跑约化省时间



